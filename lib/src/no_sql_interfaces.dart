import 'dart:async';

/// Is an interface that all collection need to implement
abstract class CollectionInterface {
  /// Allow you to delete an entry in the current collection
  Future<void> delete(Map<String, dynamic> selector);

  /// Allow you to drop all the entry in the current collection
  Future<void> drop();

  /// Allow you to find entries in the current collection
  Stream<Map<String, dynamic>> find([Map<String, dynamic> selector]);

  /// Allow you to find one entry in the current collection
  Future<Map<String, dynamic>> findOne(Map<String, dynamic> selector);

  /// Allow you to insert an entry in the current collection
  Future<void> insert(Map<String, dynamic> document);

  /// Allow you to update entries in the current collection
  Future<void> update(
      Map<String, dynamic> selector, Map<String, dynamic> document);
}

/// Is an interface that all no sql database need to implement
abstract class NoSqlDatabaseInterface {
  bool get isOpen;

  Future<void> close();

  CollectionInterface collection(String name);

  Future<void> open();
}

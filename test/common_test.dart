import 'package:database_interface/database_interface.dart';
import 'package:quiver/core.dart';
import 'package:test/test.dart';

void main() {
  group('DatabaseIdInterface', () {
    test('hashcode', () {
      final id = DatabaseId('12');
      expect(id.hashCode, hashObjects(<String>['12']));
    });

    test('toString', () {
      final id = DatabaseId('12');
      expect(id.toString(), 'DatabaseIdInterface("12")');
    });

    test('== with a DatabaseIdInterface', () {
      final id1 = DatabaseId('12');
      final id2 = DatabaseId('12');
      expect(id1 == id2, isTrue);
    });

    test('== with a String', () {
      final id1 = DatabaseId('12');
      const Object id2 = '12';
      expect(id1 == id2, isTrue);
    });
  });

  group('DatabaseException', () {
    test('message', () {
      final exception = DatabaseException('Error');
      expect(exception.message, 'Error');
    });
  });
}

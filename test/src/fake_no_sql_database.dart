import 'dart:async';

import 'package:database_interface/database_interface.dart';

class FakeCollection implements CollectionInterface {
  @override
  Future<void> delete(Map<String, dynamic> selector) async {}

  @override
  Future<void> drop() async {}

  @override
  Stream<Map<String, dynamic>> find([Map<String, dynamic> selector]) =>
      const Stream.empty();

  @override
  Future<Map<String, dynamic>> findOne(Map<String, dynamic> selector) async =>
      null;

  @override
  Future<void> insert(Map<String, dynamic> document) async {}

  @override
  Future<void> update(
      Map<String, dynamic> selector, Map<String, dynamic> document) async {}
}

class FakeNoSqlDatabase implements DatabaseInterface<CollectionInterface> {
  bool _isOpen = false;

  @override
  bool get isOpen => _isOpen;

  @override
  Future<void> close() async {
    _isOpen = false;
  }

  @override
  CollectionInterface collection(String name) => FakeCollection();

  @override
  Future<void> open() async {
    _isOpen = true;
  }
}

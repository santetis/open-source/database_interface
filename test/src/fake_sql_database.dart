import 'dart:async';

import 'package:database_interface/database_interface.dart';

class FakeTable implements TableInterface {
  @override
  Future<dynamic> query(String query) => null;
}

class FakeSqlDatabase implements DatabaseInterface<TableInterface> {
  bool _isOpen = false;

  @override
  bool get isOpen => _isOpen;

  @override
  Future<void> close() async {
    _isOpen = false;
  }

  @override
  TableInterface collection(String name) => FakeTable();

  @override
  Future<void> open() async {
    _isOpen = true;
  }
}

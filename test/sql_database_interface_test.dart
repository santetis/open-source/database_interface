import 'package:database_interface/database_interface.dart';
import 'package:test/test.dart';

import 'src/fake_sql_database.dart';

void main() {
  group('sql manager', () {
    FakeSqlDatabase fakeSqlDatabaseFactory() => FakeSqlDatabase();

    test('open', () async {
      final manager = DatabaseManager(fakeSqlDatabaseFactory);
      final db = await manager.open();
      expect(db is FakeSqlDatabase, isTrue);
      expect(db.isOpen, isTrue);
    });

    test('close', () async {
      final manager = DatabaseManager(fakeSqlDatabaseFactory);
      final db = await manager.open();
      await manager.close(db);
      expect(db.isOpen, isFalse);
    });
  });

  group('sql pool', () {
    FakeSqlDatabase fakeSqlDatabaseFactory() => FakeSqlDatabase();

    test('open', () async {
      final manager = DatabaseManager(fakeSqlDatabaseFactory);
      final pool = DatabasePool.fromManager(manager);
      expect(pool.pool, isNotNull);
      final connection = await pool.pool.get();
      expect(connection.connection.isOpen, isTrue);
      await pool.pool.manager.close(connection.connection);
      expect(connection.connection.isOpen, isFalse);
    });
  });
}

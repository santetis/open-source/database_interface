import 'package:database_interface/database_interface.dart';
import 'package:test/test.dart';

import 'src/fake_no_sql_database.dart';

void main() {
  group('no sql manager', () {
    FakeNoSqlDatabase fakeNoSqlDatabaseFactory() => FakeNoSqlDatabase();

    test('open', () async {
      final manager = DatabaseManager(fakeNoSqlDatabaseFactory);
      final db = await manager.open();
      expect(db is FakeNoSqlDatabase, isTrue);
      expect(db.isOpen, isTrue);
    });

    test('close', () async {
      final manager = DatabaseManager(fakeNoSqlDatabaseFactory);
      final db = await manager.open();
      await manager.close(db);
      expect(db.isOpen, isFalse);
    });
  });

  group('no sql pool', () {
    FakeNoSqlDatabase fakeNoSqlDatabaseFactory() => FakeNoSqlDatabase();

    test('open', () async {
      final manager = DatabaseManager(fakeNoSqlDatabaseFactory);
      final pool = DatabasePool.fromManager(manager);
      expect(pool.pool, isNotNull);
      final connection = await pool.pool.get();
      expect(connection.connection.isOpen, isTrue);
      await pool.pool.manager.close(connection.connection);
      expect(connection.connection.isOpen, isFalse);
    });
  });
}
